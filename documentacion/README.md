

[azure]: https://azure.microsoft.com/es-es/overview/what-is-azure/
[paas]: https://azure.microsoft.com/es-es/overview/what-is-paas/
[service]: https://azure.microsoft.com/en-us/services/sql-database/
[lbs_server]: http://ggeneral.bcp.com.pe/sites/gsti/seg/Lineas%20Base%20de%20Seguridad/Microsoft/Azure/PaaS/Linea%20Base%20de%20Seguridad%20-%20Azure%20SQL%20Servers.pdf
[lbs_db]: http://ggeneral.bcp.com.pe/sites/gsti/seg/Lineas%20Base%20de%20Seguridad/Microsoft/Azure/PaaS/Linea%20Base%20de%20Seguridad%20-%20Azure%20SQL%20Databases.pdf
[lineamiento]: https://confluence.lima.bcp.com.pe/display/ADT/MSQL+-+Azure+SQL

| Name        | Description |
|-------------|-------------|
| Area TI     | Arquitectura Cloud |
| CSP         | [Microsoft Azure][azure] |
| Categoria   | Base de Datos|
| Recurso     | [Azure SQL Database][service] |
| Tipo        | [PaaS][paas] |
| Código      | POAZ-MSQL |
| Mecanismo   | Script de aprovisionamiento automatizado |

<br>

# Azure SQL Database
---

Azure SQL Database es el servicio de base de datos relacional, la cual no se necesita administrar su infraestructura o licenciamiento SQL, ya que está basado en el modelo de Plataforma como Servicios (PaaS). Así mismo, este servicio está basado en la más reciente versión de Microsoft SQL Server Database Engine y además brinda un alto rendimiento, confiabilidad y seguridad para usarse en la construcción de aplicaciones empresariales.

* [Lineamientos de Uso][lineamiento]
* Línea Base de Seguridad:
    * [SQL Server][lbs_server]
    * [SQL Database][lbs_db]

El script de aprovisionamiento incluye la configuración de Línea Base de Seguridad (en adelante LBS) del componente, a excepción del punto 2 de la LBS de SQL Server el cual deberá ser coordinado por el responsable del aprovisionamiento.

<br>

# Módulo Terraform
---

### **Descripción**
Este proyecto esta diseñado para ser ejecutado como parte de una __Arquitectura de Referencia__ y/o de forma individual.

<br>

### **Requisitos**

    * azure-cli >= 2.0.65
    * terraform >= v0.12.6
    * git >= 2.7.4

<br>

# Configuración de Variables
---

### **Conceptos Generales**

* **Variables públicas globales**:
	* Declaración: `variable nombre_var {}`
	* Deben ser ingresadas como argumento en la ejecución del __terraform apply__.
	* Normalmente son proveídas por el usuario o propagadas desde una __Arquitectura de Referencia__, por tal motivo, no tienen valor por default.

* **Variables públicas para sobreescritura**:
    * Pueden ser ingresadas o no como argumento en la ejecución del __terraform apply__ ya que cuentan con valor por defecto.

* **Variables Locales o privadas**:
	* Declaradas dentro del bloque `Private module variables`.
	* **Son de uso interno y no deben ser extendidas y/o modificadas**.

* **Variables para sobreescritura**:
	* Poseen un valor por `default` dentro del bloque de definición de variable `{***}`.
	* Puede modificarse el valor por default usando un archivo `.tfvars` o ingresar como argumento en la ejecución de __terraform apply__.

<br>

### **Variables**

| Nombre | Requerido  | Descripción |
| ------ | :--------: |-------------|
| cod_app |SI| Especifica el código de la aplicación del portafolio de aplicaciones. El código de aplicación consta sólo de 4 dígitos. |
| cod_env |SI| Especifica el ambiente del servicio. `Valores permitidos: d = Desarrollo, c = Certificación, p = Producción` |
| cod_loc |SI| Especifica la región del servicio. `Valores permitidos: eu2 = East US 2, cu1 = Central US` |
| num_rsc |SI| Especifica el correlativo del servicio. `Valores permitidos: desde 01 hasta 99`|
| num_rsgr_infr |SI| Especifica el correlativo de la infra base. `Valores permitidos: desde 01 hasta 99`|
| msql_db_name |SI|  Especifica el nombre de la Base de Datos. El nombre no debe contener caracteres especiales o palabras reservadas. |
| msql_db_size_type |SI| Especifica el tier de la BD. `Valores permitidos: basic, standard, GeneralPurpose, etc`, ver NOTA |
| msql_db_size |SI| Especifica el tipo de tamaño(size) del tier de la BD. Para mayor información, revisar los [Lineamientos de Uso][lineamiento] del componente.`Valores permitidos: basic, s1, s2, etc`, ver NOTA |
| msql_db_repl_size |NO| Especifica el tipo de tamaño(size) del tier de la BD secundaria. Para mayor información, revisar los [Lineamientos de Uso][lineamiento] del componente.`Valores permitidos: basic, s1, s2, etc`, ver NOTA, NOTA2 |



* Donde:
    * cod_env: 
        * d = Desarrollo
        * c = Certificación
        * p = Producción
    * cod_loc: 
        * eu2 = East Us 2
        * cu1 = Central Us

<br>

> _**NOTA**: Para ver todas las opciones disponibles para el size del SQL Server se debe ingresar al cloudshell del portal y ejecutar el siguiente comando: `az sql db list-editions -l <region a aprovisionar> -o table` siendo las columnas **Edition**(msql_db_size_type) **ServiceObjective**(msql_db_size) los dato a obtener._

> _**NOTA 2**: En el caso que se desee cambiar el tier del azure sql cuando esta habilitado la **multiregion** se debera seguir las especificaciones del siguiente link: https://docs.microsoft.com/en-us/azure/azure-sql/database/migrate-dtu-to-vcore  ._

# Multiregión
---

**Tener en consideracion: para desplegar el componente en multiregion, se debe haber desplegado los recursos de la infraestructura base en ambas regiones, de caso contario abra errores de aprovisionamiento.** <br>

Para el caso de un despliegue multiregión deberá tener en cuenta el formato de las siguientes variables. En el siguiente ejemplo se muestran las variables para desplegar en la región eu2 y cu1:

**Ejemplos de despliegue cod_loc**

**Una Region:**

cod_loc = ["eu2"]

**Multiregion**

cod_loc = ["eu2","cu1"]

**Ejemplos de despliegue size**

**Size Basico:**

msql_db_size= "basic"

msql_db_size_type= "basic"

**Size Standard:**

msql_db_size= "s1" (los parametros para standard pueden ser s1, s2 ó s3)

msql_db_size_type= "standard"

<br>


# Tags
---

La asignación de los "tags" se realiza de forma automática con los siguientes datos:

<br>

| Tag | Descripción |  Valores  |
| ---------| ----------- |-----------|
| **provisionedBy** |  Ejecutor y método de aprovisionamiento  | IBM-Terraform |
| **provisionedFrom**  | Método de despliegue | Msql-Module |   
| **operation**  | Proveedor de operación  | IBM |   
| **codApp**  | Código de aplicación   | Código de 4 dígitos  |
| **environment**  | Ambiente de la infraestructura de referencia  |  **DESA** (Desarrollo) <br> **CERT** (Certificación) <br> **PROD** (Producción) | 

<br>

# Instrucciones de Aprovisionamiento
---
# Aprovisionamiento Mediante File TFVARS
```bash
##Descarga del módulo - modificar el campo <matricula> (sin <>):
> git clone https://<matricula>@bitbucket.lima.bcp.com.pe/scm/nube/azu-msql-module.git

##Validacion del file var.tfvars
Dentro del modulo se encontrara el file var.tfvars, el cual tiene las variables de despliegue. Ejemplo:

cod_app = "iac2"
cod_loc = ["eu2"]
cod_env = "d"
num_rsc = "01"
msql_db_name= "dbNameDemo"
msql_db_size= "s1"
msql_db_size_type= "standard"

##Iniciar terraform:
> terraform init

##Validar terraform:
> terraform validate

##Validar ejecución (se solicitará el ingreso de las variables globales):
> terraform plan

##Ejecución de ejemplo:
> terraform apply -var-file=tfvars
```
# Aprovisionamiento Mediante Job de Jenkins
```bash
##Dirigirce al directorio 
> https://<matricula>@bitbucket.lima.bcp.com.pe/scm/nube/azu-msql-module.git

##Para el despliegue utilizar el contenido de los archivos de la carpeta provisioning-example.

Archivos:
 * main.tf
 * variables.tf
 * output.tf
 * var.tfvars

 ##Una vez copiado el contenido, se modificara el del archivo var.tfvars segun el despliegue. Ejemplo:

cod_app = "iac2"
cod_loc = ["eu2"]
cod_env = "d"
num_rsc = "01"
msql_db_name= "dbNameDemo"
msql_db_size= "s1"
msql_db_size_type= "standard"

```
<br>

* **Outputs**

    Por default se ha configurado las siguientes salidas para revisar la información del recurso aprovisionado: 
    `applicationCode, environment, subscription, sql_server_name, server_endpoint, database_name, resourceGroup`. En caso de requerir agregar más campos se debe modificar el archivo **[output.tf](output.tf)**

    <br>

    * Ejemplo de salida:

        ```bash
        Apply complete! Resources: 9 added, 0 changed, 0 destroyed.

        Outputs:

        applicationCode = nube
        database_name = dbNameDemo
        environment = DESA
        resourceGroup = RSGREU2NUBED01
        server_endpoint = msqleu2nubed02.database.windows.net
        sql_server_name = msqleu2nubed02
        subscription = Eureka/1a1a1a1a-1a1a-1a1a-1a1a-1a1a1a1a1a1a
        ```

<br>

# Lineamientos de Seguridad (Configuración Manual)
---

#### **Luego de culminar el correcto aprovisionamiento del componente, el responsable del aprovisionamiento debe coordinar la ejecución de los cambios mencionados en:**

* Cambio de password del Server Admin (SQL Server): punto __2__ del documento [Línea Base de Seguridad - Azure SQL Servers][lbs_server].
* Cambio de tamaño en GB de la base de datos (SQL database).


<br>

# Destrucción del Aprovisionamiento
---

En caso de presentar errores durante la implementación o se desee eliminar el recurso aprovisionado mediante el uso del módulo provisto en este repositorio, se tiene el comando `terraform destroy`. Terraform determinará el orden en que los recursos deben ser destruidos respetando las dependencias presentes. El prefijo `"-"` indicará los recursos que se destruirán.

> _**NOTA**: Se debe conservar el archivo `terraform.tfstate` y se debe ingresar las mismas variables con la que fue aprovisionado el componente._

* **Instrucciones**:

    ```bash
    ##Ubicarse en la carpeta del módulo:
    > cd azu-msql-module

    ##Ejecución de ejemplo:
    > terraform destroy -var-file=tfvars

    ##Luego de la ejecución del comando anterior, se mostrará la cantidad y la información de los recursos a destruir, en caso de estar conforme con la destrucción ingresar "yes".
    ```

<br>

* **Outputs**

    * Ejemplo de salida:

        ```bash
        Destroy complete! Resources: 9 destroyed.
        ```
