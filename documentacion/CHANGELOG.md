# Changelog

Los cambios del módulo serán documentados en este archivo. Utiliza [Semantic Versioning][01].

## [x.y.z] - aaaa-mm-dd
-----

### Added

- Agrega funcionalidades nuevas.

### Changed

- Para los cambios en las funcionalidades existentes.

### Fixed

- Para correcciones y bugs.

### Deprecated

- Para indicar que una característica está obsoleta y que se eliminará en las próximas versiones.

### Security

- Cambios en Linea Base de Seguridad (LBS).

### Removed

- Para las características en desuso que se eliminaron en esta versión.

## [2.0.0] - 2020-08-04
-----


### Added

- Soporta cambio de Tamaño de DB hacia una Tier distinta ej: Standard S1 a General Purpose GP_Gen5_2.
- Se crea Changelog.md
- Soporta Multiregion (Geo Replicacion).
- Variable msql_db_size_type : Especifica el tier del tamaño de la BD.


### Changed

- Versión provider azurerm : 2.20.0.
- Refactorización de código.
- Actualizacion de variables globales como tipo object: app_code, env_code, env_name, provisioned_by.

## [1.0.0] - 2020-05-05
-----

### Added

- **initial-release**
- Incluye configuraciones de Linea Base de Seguridad publicado el 2020-01-28 (LBS v1.2).
- Incluye configuraciones según lineamientos de uso definido por arquitectura.
- Soporta solo SQL server standalone
- Soporta cambio de Tamaño de DB en una misma Tier ej: Standard S1 a S2
- Desarrollado con terraform versión : 0.12
- Versión provider azurerm : 1.44.0.



[x.y.z]: https://github.com/olivierlacan/keep-a-changelog/compare/v1.0.0...v1.1.0
[2.0.0]: https://github.com/olivierlacan/keep-a-changelog/compare/v1.0.0...v1.1.0
[1.0.0]: https://github.com/olivierlacan/keep-a-changelog/compare/v0.3.0...v1.0.0
[0.3.0]: https://github.com/olivierlacan/keep-a-changelog/compare/v0.2.0...v0.3.0
[0.2.0]: https://github.com/olivierlacan/keep-a-changelog/compare/v0.1.0...v0.2.0


[01]: https://semver.org/spec/v2.0.0.html