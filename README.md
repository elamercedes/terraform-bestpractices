# Estandar de Scripting - Terraform  

Este documento describe el estandar y buenas prácticas para crear módulos de terraform, está dividido en las siguientes secciones:


1. [GitFlow](##1.-GitFlow)
2. [Semántica de Versiones](##2.-Semántica-de-Versiones)
3. [Módulos](##3.-Módulos)
    * 3.1 [Estructura](###3.1.-Estructura)
    * 3.2 [Clasificación](###3.2.-Clasificación)
    * 3.3 [Implementación](###3.3.-Implementación) 
    * 3.4 [Convenciones de Nomenclatura](###3.4-Convenciones-de-Nomenclatura)
    * 3.5 [Resources](###3.5.-Resources )
    * 3.6 [Providers](###3.6.-Providers)
    * 3.7 [Variables](###3.7.-Variables)
    * 3.8 [Locals](###3.8.-Locals)
    * 3.9 [Data](###3.9.-Data) 
    * 3.10. [Outputs](###3.10.-Outputs)  
4. [Módulo Orquestador](##4.-Módulo-Orquestador)
    * 4.1 [Capas](###4.1-Capas)
    * 4.2 [Dependencias](###4.2.-Dependencias)
    * 4.3 [Variables](###4.3.-Variables)
5. [Documentación](##5.-Documentación)
    * [README.md](##5.-Documentación)
    * [CHANGELOG.md](##5.-Documentación)

6. [Continues Integration Test Pipelines](##6.-Continues-Integration-Test-Pipelines)
7. [Links de Referencia](##7.-Links-de-Referencia)

## 1. GitFlow

+ Se utiliza el GitFlow definido por DevSecOps para el desarrollo de software. Utilizando el [pipeline FreeStyle][1], el cual  promueve código a la rama release, master y crea el TAG correspondiente, previamente subiendo el código (.tar) a artefactory.

+ Para el Modelo Operativo V2, se agregará un stage en el pipeline para publicar el módulo en [Private Module Registry][2] (PMR). Sólo a través de este se podrá consumir módulos. En el caso se promueva una versión posterior a la publicación, este sincronizará automáticamente con PMR, el Tag que reconocerá tiene la siguiente semántica : x.y.z. 

+ El proyecto Bitbucket donde se versionará los modulos es [IAAC][3], sobre el cual se restringirá la creacion de tags con la semántica x.y.z. Con el fin de que no se publiquen módulos en PMR sin los controles definidos por el Banco.

>**Nota :** Para la promoción de código a master, se solicitará a través de una solicitud [MVP][4] con criticidad 1. Pasando por las conformidaddes del LT, PO y Analista de seguridad del Squad responsable de la tecnología.

### Uso de Ramas

|  Ramas |   Descripción  
|:------------------------------- |:-------------------------------- |
| master |   Siempre tendrá la última versión del módulo, cualquier commit debería estar antes en develop.|
| release | Se utiliza para el siguiente código en producción, incorporándolo luego a master. Contiene Release Candidate (RC), creado desde develop. |
| develop |  Contiene el código que conformorá la siguiente versión planificada a master. Se denomina rama de integración, previamente el código debe pasar por revision par y pruebas satisfactorias antes de hacer merge, a través de pull request aprobado por el LT y/o revisores del Squad.|
| feature |  Para rear nuevas caracteristicas o funcionalidades, una vez terminada se incorporá a la rama develop y será eliminada.

>**Nota :** La rama **feature** se debe crear desde Jira a través de una historia con el fin de tener la trazabilidad del cambio, nomenclatura --> *feature/{EUR-123}-{name-jira-story}*|

### Commits

El uso frecuente de commits es bueno, pero siempre debe ser a criterio de cada uno para poder describir los cambios importantes que se realizan para implementar el requerimiento o el objetivo de una historia. Este debe tener una estructura y consistencia, que permitirá un fácil mantenimiento del código. Consideraciones a tomar en cuenta:

* El título del commit contendrá el código de la historia de Jira.
* No termines la línea del título con un punto.
* Utiliza el modo imperativo, significa usar verbos para dar una orden: actualiza, agrega,elimina ,etc.

Ejemplos:
 
     EUR-123-Versión de lanzamiento 1.0.0
     EUR-123-Elimina métodos obsoletos
     EUR-123-Actualiza la documentación inicial
     EUR-123-Refactoriza módulo para facilitar la lectura
     EUR-445-Agrega variable lista para aceptar múltiples regiones 


## 2. Semántica de Versiones
Seguir las recomendaciones de [Semantic Versioning][5]

La semántica a utilizar es:

![alt text](./img/semantica.png)

Además de especificar la relacion con releases anteriores, el número de versión  suele denotar de alguna forma el grado de estabilidad del mismo.

- Alpha  --> 1.2.1-alpha
    - Todavía en desarrollo (no está completa)
    - Inestable.
    - Testeado solo en forma unitaria.
    - En general, disponible solo en forma interna (ej, para el equipo de testing)
    - Suele terminar con “Congelamiento de código” para alcanzar Feature Complete.

- Release Candidate (RC) --> RC-1.2.1-CERT-yyyymmddhhmmss
    - Una versión con potencial de convertirse en la versión final (a menos que aparezcan bugs bloqueantes)
    - Funcionalidad completa
    - Bugs resueltos.
    - Baja probabilidad de apariencia de bugs.
    - Cierto grado de estabilidad, aunque todavía se esperan algunos problemas.
    - Intención: recibir feedback de los usuarios, sin embargo, éste debe saber que es una versión preliminar.

- Release  --> 1.2.1
    - Un RC "graduado".
    - Versión productiva final.
    - Estable
    - Baja probabilidad de apariencia de bugs.
    - Disponible para ser distribuido ámpliamente.
    - Liberado a master

## 3. Módulos
### 3.1. Estructura

Un módulo terraform esta asociado a un repositorio Bitbucket del proyecto [IAAC][3], versionable y modularizado. Debe tener la siguiente estructura: 


|  Artefacto |   Descripción  
|:------------------------------- |:-------------------------------- |:-------------------------------- |
| /devops/jenkins |   Folder de despligue, contiene los pipelines para promover el código y tageo del módulo.
| /examples | Folder de ejemplos, código terraform, indica la manera de como combinar los módulos: casos de uso.
| /test |  Folder de pruebas, código GO para automatizar pruebas , deploya y valida cada example.
| main.tf|  Código de implementación: declara las especificaciones de cada característica  del recurso.
| variables.tf|  Definición de variables de entrada.
| outputs.tf|  Definición de variables de salida.
| README.md |  Documentación técnica del recurso, indica como usar el módulo. Formato de ejemplo [README.md][6].
| CHANGELOG.md |  Detalla el historial de versiones. Formato de ejemplo [CHANGELOG.md][7].
| version | Archivo que indica la versión del módulo respetando la semántica de versiones.  Es un parámetro que se utiliza en el pipeline /devops/jenkins.

#### Ejemplo :

    terraform-azu-<SERVICE_CODE>
      |-- /devops/jenkins
      |-- /examples
      |-- /test
      |-- main.tf 
      |-- variables.tf      
      |-- outputs.tf   
      |-- README.md
      |-- CHANGELOG.md
      |-- version


Donde:

* terraform-azu-<SERVICE_CODE> : Es el nombre del repositorio Bitbucket, dentro del proyecto [IAAC][3], gestionado por la Mesa Cloud.

* <SERVICE_CODE> : Son las siglas  (4 dígitos) para cada tipo de recurso Azure definidos por el área de Gobierno y [Estandares TI][8].

### 3.2. Clasificación

Pueden ser de 3 tipos:

* Módulo Individual :  Es la pieza mas pequeña denominado módulo hijo, asociado a un recurso azure que soporta ciertas características. Crea un conjunto plano de recursos y la relacion entre ellos.
* Módulo Arquetipo : Composición de módulos individuales, ensambla varios bloques de módulos para producir una pieza mas grande . 
* Módulo Arquitectura de Referencia: Composicion de módulos arquetipos. Crea una arquitectura completa Web, Mobile, Web Simple definido por Arquitectura TI.

Según la [definición de los arquetipos][9] por el área de Arquitectura TI, algunos moóulos individuales pueden ser de un tipo arquetipo.

> El objetivo es diseñar  piezas que puedan ser independientes y estas puedan ser consumidas por un módulo u orquestador , con el fin de ensamblar desde una funcionalidad especifica hasta una solucion completa de negocio (Aplicaciones Web, Mobile, etc).

![alt text](./img/modulos.png)

### 3.3. Implementación

Para la implementación de un módulo terraform, en el archivo de configuración  main.tf , se debe considerar:

* Se recomienda el uso de [mapas][17], listas, mapa de objetos.
* Uso de [for_each][10] para la creación de recursos dentro de un módulo individual teniendo como llave principal la **REGION**. Esto con el fin de abstraer la complejidad a nivel de módulo y simplificar a nivel de orquestador final.
* Configuración del recurso según [lineamiento de uso][11] definido por Arquitectura cloud..
* [Linea Base de Seguridad][12] publicado por Arquitectura de Seguridad.
* No usar parámetros para propiedades que modifican la linea base de seguridad.
* Se sugiere el uso de tags para organizar de forma lógica los recursos asociados a la subscripcion.

### 3.4. Convenciones de Nomenclatura

+ El nombre de las variables, locals debe ser en **ingles.**
+ El prefijo `is` debería usarse para las variables y locals booleanos. 

    Ejemplo:

        is_msql_enable_vulnerability = true  //Enable Vulnerability Assessment Settings.
 
+ Las variables booleanas negadas deben evitarse.
+ La forma plural debería utilizarse en nombres que representen una colección de objetos, listas, mapas. 

    Ejemplo:

        variable locations_code {
            description = "Lista de códigos de regiones a desplegar. Formato: ["eu2","cu1"]"
            type = list(String)
        }
    
+ Deben evitarse las abreviaciones en los nombres, excepto las frases difundidas que se conocen más por su abreviación, nunca escribas:

        cod                         en vez de   code
        kv                          en vez de   keyvault
        svc                         en vez de   service
        HypertextMarkupLanguage     en vez de   html


+ **Comenta tu código:** para facilitar las modificaciones y mantenimiento, pero deberían contener sólo información relevante para la lectura y comprensión del código. Debe evitarse la duplicidad de información dada por el propio código. Considerar: 
    + Resaltar partes del codigo que parecen irrelevantes pero que son importantes.
    + Advertencias sobre consecuencias.
    + Comentarios para aclarar alguna variable con un nombre especificado forzado por una nomenclatura no elegida por le programador.
    + TODO comments. Deuda técnica que queda pendiente corregir.
+ Los comentarios ayudan a comprender el proposito del resource que se esta aprovisionando.
+ **Código confuso:** El código confuso no debería comentarse sino reescribirse. En general, el uso de los comentarios debería minimizarse haciendo el código auto documentado con elecciones apropiadas de nombres y estructuras lógicas explícitas.
+ **Idioma:** Todos los comentarios deberían escribirse en inglés.
+ **Formateo:** Formatear el código terraform para facilitar la lectura con el comando [`terraform fmt`][14] siguiendo el [estilo de convenciones][15] del mismo.
+ **Nomenclatura :** Considerar las [nomenclaturas cloud][16] definido por arquitectura para el nombre de algunos servicios . Para el código terraform tomar en cuenta lo siguiente:

     

    | Tipo     | Convención | Nomenclatura                      | Ejemplos                  |
    |:---------|:---------- |:--------------------------------- |:------------------------- |
    | Variable | lower_snake_case | <SERVICE_CODE>_<CUSTOM_CONF_NAME> | msql_db_size, msql_db_name|
    | Locals   | lower_snake_case | Según criterio, excepto los [casos definidos][10].| client_app_id, kubernetes_version|
    | Outputs | lower_snake_case | <SERVICE_CODE>_<CUSTOM_CONF_NAME> | msql_server_name, msql_server_endpoint|
    | Tags | lowerCamelCase | Según criterio. | provisionedBy, lbsVersion|
    | Resource | lower_snake_case | resource <RESOURCE_TYPE> <RESOURCE_NAME> { ... } | resource "azurerm_app_service_plan" "wapp" { ... }|
    | Module | lower_snake_case | module <SERVICE_CODE> { ... } | module "aksv" { ... } |
    | Azure Services | lowercase | <SERVICE_CODE> + <LOCATION_CODE> + <APPLICATION_CODE> + <ENV_CODE> + <RESOURCE_SEQUENTIAL> | msqlcu1iac2d01, akvteu2iac2c01 |
    | Resource Group | UPPERCASE | RSGR + <LOCATION_CODE> + <APPLICATION_CODE> + <ENV_CODE> + <RSGR_SEQUENTIAL> +  | RSGREU2IAC2C01, RSGRCU1IAC2D01|

Donde:

+ **<SERVICE_CODE>:** Son las siglas  (4 dígitos) para cada tipo de recurso Azure definidos por el area de Gobierno y Estandares TI, ver [aqui][2]. Para `local` es opcional, excepto los valores estáticos definidos.
+ **<RESOURCE_TYPE>:** Es el nombre del tipo del recurso en terraform, segun provider azurerm.
+ **<RESOURCE_NAME>:** Es el nombre del recurso azure en terraform, para utilizarlo como referencia.
+ **<LOCATION_CODE>:** Código de 3 letras para las regiones donde se deplegará los servicios. , puede ser  "eu2" o "cu1".
+ **<APPLICATION_CODE> :** Especifica el código de de 4 dígitos de la aplicación o proyecto, según [portafolio de aplicaciones][10].
+ **<ENV_CODE> :** Especifica el código de ambiente del recurso de un dígito. Puede ser "d", "c" o "p".
+ **<RESOURCE_SEQUENTIAL>:** Especifica el número sequencial del recurso, puede ser desde "01" hasta "99".
+ **<RSGR_SEQUENTIAL>:** Especifica el número sequencial del grupo de recurso, puede ser desde "01" hasta "99".

### 3.5. Resources 

* Crea varios recursos a la vez  con [for_each][10], definiendo los valores de cada instancia con un solo llamado y teniendo como llave principla la **REGION**. A partir de terraform versión 0.12.6 podemos usar la función `for_each` en lugar de `count` en la creación de recursos.

* Nomenclatura para el nombre del bloque de recurso:


        resource "<RESOURCE_TYPE>" "<RESOURCE_NAME>"
        {

        }   

Donde:

* `<RESOURCE_NAME>` : Corresponderá al código del servicio de 4 dígitos `<SERVICE_CODE>` del módulo a implementar. Todos los bloques de recursos que componen dicho  módulo tendrán el mismo nombre, excepto los de tipo "null_resource" y recursos duplicados.

    Ejemplo módulo WAPP:

        resource "azurerm_app_service_plan" "wapp" { }
        resource "azurerm_app_service" "wapp" { }
        resource "azurerm_template_deployment" "wapp" { }


* Cada argumento de configuración debe asociarse a una **variable**  o **local** , a excepción de los casos que defina el provider.
* Dependencia entre recursos, se puede dar de dos formas:
    * Uso de Depends on, para forzar la dependencia entre recursos.
    * Uso de los argumentos de configuración del recurso principal, en otro bloque de recurso.

    ![alt text](./img/dependencia1.png)

> **Nota** : En terraform >= 0.13 , es posible utilizar  [depends-on sobre módulos][13] a nivel de `orquestador final`, para esto es necesario definir el provider .



### 3.6. Providers

+ Los Providers deben estar siempre al inicio del Main.tf para una rápida identificación.
+ Se definen en el orquestador final.
+ Cada cierto tiempo se deberá revisar si existe alguna actualización del provider para la preparación de actualización del módulo.
+ La versión del provider a utilizar será el penultimo release, excepto en los casos que sea necesario utilizar el último para incluir un fix.

    Contendrá lo siguiente:

    main.tf

        provider "azurerm" {
            features {}
            version = "=2.20.0"
        }

+ Para el caso del provider `terraform` se definirá desde el workspace correspondiente.

> NOTA: Para el caso de módulo AKSV, se debe considerar manejar un archivo providers.tf en el nivel del `orquestador final`.

### 3.7. Variables

+ Toda variable debe tener  su definición en el archivo variables.tf, incluyendo : 
    - description  --> Descripción corta y el formato de entrada.
    - type
    - default  --> Solo para variables opcionales.

+ Toda variable debe tener [validaciones][17] de entrada tanto globales como privadas, sobre todo para los que ingresa el usuario. El mensaje de error debe escribirse en inglés.

    variables.tf

        variable "globals" {
            description = "Variables globales: app_code, env_code, env_name, provisioned_by."
            type = object({
            app_code      = string
            env_code      = string
            env      = string
            provisioned_by      = string
            })

            validation {
                condition = length(var.globals.app_code) == 4
                error_message = "Application Code must be a 4 letters string."
            }
        }


Las variables se distribuye de la siguiente manera:

+ Variables:
    * **Privadas** : Las variables privadas son aquellas que se utilizan para indicar las configuraciones adicionales que se realizarán en el recurso a aprovisionar, tambien utilizadas por el usuario final de infraestructura. (ej, Variable para indicar segmento de red para el subnet de un AKS).

    * **Globales** : Utilizadas en todos los módulos, para construir según nomenclatura el nombre, locación y ambiente del recurso azure, Tags, etc.

        * La Descripción en *cursiva* se debe colocar en la definición de las variables del módulo, para mantener consistencia. 

        |Variable Global| Descripción | Valores permitidos | Default |Tipo |
        |:---- |:---- |:---------------|:--------------------|:------------|
        | globals|*Variables globales: app_code, env_code, env_name, provisioned_by : ${module.app.globals}*  Es un conjunto de variables , que requiere todos los módulos.| {app_code = "<APPLICATION_CODE>", env_code = "<ENV_CODE>", env_name = "<ENV_NAME>", provisioned_by = "<PROVISIONED_BY>"}| Según lo configurado en el workpace de la aplicación. Este valor prevalece sobre cualquier valor ingresado por el usuario.|Object de strings|
        | globals.app_code|Especifica el código de 4 dígitos de la aplicación o proyecto, según [portafolio de aplicaciones][18].| String de 4 dígitos  |Según configuración de workspace.|Object (String)|
        | globals.env_code|Especifica el código de ambiente del recurso de un dígito. El cual será definido en cada workspace de TFE .| "d", "c"  o  "p"    |Según configuración de workspace.|Object (String)|
        | globals.env_name | Especifica el nombre del ambiente de 4 dígitos. El cual será definido en cada workspace de TFE .| "DESA", "CERT", "PROD" | Según configuración de workspace.|Object (String)|
         | globals.provisioned_by |  Especifica la identificación de la herramienta que aprovisiona el recurso. El cual será definido en cada workspace de TFE  o Imagen Docker que instancie Jenkins.|"tfe", "jenkins" |Según configuración de workspace o Jenkins|String|
        | locations_code |  *Lista de códigos de regiones a desplegar . Eje: ["eu2","cu1"]*| [ "eu2", "cu1" ]| -|List (String)|
        | resource_sequential |  *Especifica el número sequencial del recurso.*| Desde 01 hasta 99| "01"|String|
        | rsgr_sequential |  *Especifica el número sequencial del grupo de recurso*| Desde 01 hasta 99| "01"|String|


### 3.8. Locals


+ Los locals son expresiones que tienen un valor asignado, estos se usan para evitar el uso excesivo de variables. En el archivo variables.tf utilizamos los locals para los siguientes casos.
    -	Valores estáticos definidos: Estos valores contienen las nomenclaturas para los códigos de los servicios establecidas como estandar . Cabe resaltar que no todos los locals indicados a continuación se usan en un módulo.

        |  Local |   Descripción | Nomenclatura | 
        |:------- |:------------------- |:------------ |
        | <SERVICE_CODE>_code |  Local usado para indicar el código del servicio de 4 dígitos.|  `codb_code = "codb"`, `aksv_code = "aksv"`, `rsgr_code = "rsgr"`, `lgan_code = "lgan"`, `vnet_code = "vnet"`, `akvt_code = "akvt"`. | 
        | <SERVICE_CODE>_name |  Local usado para indicar el nombre del servicio según nomenclatura. |  local.<SERVICE_CODE>_code + var.locations_code + var.globals.app_code + var.globals.env_code + var.resource_sequential. --> `aksv_name = aksveu2iac2d01` | 
        | lgan_type_code |  Local usado para indicar el código correspondiente del Log Analytics de infraestructura o seguridad.| "infr", "segu"| 
        | rsgr_infr_name |  Local usado para indicar el nombre del grupo de recurso infra.|  local.rsgr_code + var.locations_code + var.globals.app_code + "F" + var.rsgr_sequential. --> `rsgr_infr_name = RSGREU2IAC2F01`|
        | lgan_infr_name |  Local usado para indicar el nombre del servicio azure log analytics de infra. | local.lgan_code + `region` + var.globals.app_code + local.lgan_type_code + var.resource_sequential. -->  `lgan_infr_name = lganeu2iac2infr01` | 
        | lgan_segu_name |  Local usado para indicar el nombre del servicio azure de log analytics de seguridad. | local.lgan_code + `region` + var.globals.app_code + local.lgan_type_code + var.resource_sequential. -->  `lgan_infr_name = lganeu2iac2segu01` | 
        | tags |  Considerar los siguientes: **provisionedBy** , **operation** , **codApp** , **environment** , **lbsVersion** (Versión del documento [LBS][12] actual sobre la cual se desarrolla/modifica el módulo en ese momento), **moduleVersion** (La versión x.y.z del módulo). | Seguir la convención de nombres lowerCamelCase. Ejemplo: miBlogDeDesarrollo --> `provisionedBy = var.provisioned_by`, `operation = "IBM"` , `codApp = var.app_code`  , `environment = var.env` , `lbsVersion = "1.1"` , `moduleVersión = "2.0.0"`| 
      
    -	Composición de nombre a partir de variables y otros locals estáticos (Nomenclaturas establecidas por el banco).
    -	Declaración de valores obtenido con data source.
    -	Condicionales.

### 3.9. Data

+ Los **data source** se utilizan para obtener valores de recursos que ya están creados o se crearán durante el aprovisionamiento.
+ Existes varios servicios que reportan Logs hacia: lgan-segu, lgan-infr, stac-segu o stac-infr. Es necesario obtener IDs de estos servicios (lgan, stac) para configurar el diagnostic settings, los cuales se obtendrán a través de un `data` utilizando `for_each`, de la siguiente manera:

    variables.tf

        local.lgan_code = "lgan"
        local.lgan_type_code = "<TYPE>"  //Según lo que indica LBS.

        data "azurerm_log_analytics_workspace" "lgan_<TYPE>"{
            for_each = {  for region in var.locations_code:
                 region => { name = format("%s%s%s%s%s",lower(local.lgan_code), lower(region), lower(var.globals.app_code), lgan_type_code, var.resource_sequential)  
                             rsgr = format("%s%s%s%s%s",upper(local.rsgr_code),upper(region) , upper(var.globals.app_code),"F", var.rsgr_sequential)
                            }    
            }
            name                = each.value.name
            resource_group_name = each.value.rsgr
        }
    
    Resultado:

    ![alt text](./img/data_lgan.png)
    
    En este caso vemos que se desea obtener información del log analytics de <TYPE>  utilizando for_each, el cual se obtiene una estructura tipo mapa de objetos, que nos permite obtener los datos :  nombre de log analytics y nombre del RG de infraestructura.

    Donde:
    <TYPE> : Puede tener los valores "infr" o "segu".
    
    Para obtener los datos del data anterior:

        local.lgan_<TYPE>_name = azurerm_log_analytics_workspace.lgan_<TYPE>[region].name
        azurerm_log_analytics_workspace.lgan_<TYPE>[region].rsgr
        azurerm_log_analytics_workspace.lgan_<TYPE>[region].id

+ Se recomienda utilizar el data con for_each, teniendo como llave principal la REGION. El punto anterior es un ejemplo de uso.

### 3.10. Outputs

+ Los outputs nos permiten obtener información de un recurso después de aprovisionarse.
+ Cuando se define un output debe incluirse **Description**.
+ Cada output se le asigna una **Etiqueta**, indicador que representa el dato a mostrar:

    Ejemplo:

        output "msql_server_name" {
            value = {for region in keys(azurerm_sql_server.msql): region => azurerm_sql_server.msql[region].name}
        }
    
    Resultado:

        {   eastus2 = msqleu2iac2d01,
            centralus = msqlcu1iac2d01  }

    Tenemos:
    
    +	**Outputs Generales :** Son aquellos que solo se definen en el orquestador final del usuario. 

        |Output General | Value|
        |---------------|-------|
        |application_code | module.app.app_code | 
        |environment |  module.app.env_name|
        |subscription_name |  ${data.azurerm_subscription.current.display_name}|
        |resource_group | module.ienv[region].rsgr_name| 
     
    Donde:
    
        `ienv` es el módulo arquetipo infraestructura de ambiente, orquestado por el usuario final para la version 2 de infra.

    +	**Outputs Obligatorios :** Son aquellos que se definen en el módulo individual para las pruebas automatizadas.

        |Output Obligatorio | Value|
        |---------------|-------|
        |subscription_id | ${data.azurerm_subscription.current.subscription_id} |
        |resource_name |  <RESOURCE_TYPE>.<RESOURCE_NAME>[region].name|
        |resource_id |  <RESOURCE_TYPE>.<RESOURCE_NAME>[region].id|

        Considerar el resource_name principal del módulo, que permita validar  que el resto de recursos se crearon satisfactoriamente, Ejemplo: Function necesita un storage para aprovisionarse correcrtamente.

    +	**Outputs Adicionales :** A veces se requiere agregar más outputs para detallar datos necesarios para el usuario final. Ejemplo. Cadenas de conexión, flavor, segmentos de red, Endpoints, hosts,etc. Estos outputs deben ser de tipo [map][17].


## 4. Módulo Orquestador

El módulo orquestador utiliza varios módulos para su aprovisionamiento tiene una estructura similar a un módulo simple (Main.tf, variables.tf y Output.tf), con la diferencia que extrae las variables de los módulos a utilizar para el ingreso de datos. Por lo tanto se deben respetar las buenas practicas indicadas en los puntos pasados.

Dicho esto se detallan a continuación las buenas practicas particulares para un módulo orquestador.

### 4.1. Capas

Terraform nos recomienda mantener una máximo de 3 Capas de módulos jerárquicos, esta limitación se debe a que se desea evitar una complejidad innecesaria.

Esta “complejidad innecesaria” se puede explicar yendo capa por capa:

-	Capa 1: Módulo individual. ej. terrafor-azumsql, terraform-azu-aksv.
-	Capa 2: Módulo arquetipo o arquitectura de referencia. ej. terraform-azu-xxxx, terraform-azu-aweb.
-	Capa 3: Módulo orquestador final : Implementado por el "usuario final de infraestructura". azu-<APPLICATION_CODE> .

En la primera capa se tienen los módulos individuales, los cuales crean 1 o mas recursos en específico, la segunda capa usa estos módulos individuales para aprovisionar varios de ellos al mismo tiempo, para ello los declara en su Main.tf y trae las variables de cada módulo individual que necesite un dato. Finalmente en la capa 3 ,en el main.tf, se declara el módulo arquetipo/arq. referencia de la capa 2 y se vuelven a traer las variables de la Cap1.

Dicho esto supongamos que queramos crear una “capa 4” esta seria exactamente igual al de la capa 3 y no agregaría algún beneficio. 

### 4.2. Dependencias

Asi como las dependencias entre recursos, también se puede generar dependencias entre recursos de un modulo a otro, esto se hace a través de los outputs.

Ejemplo:

Tenemos el output que nos trae la IP Egress del AKS y se quiere usar para agregarlo en una regla de firewall del Redis Cache:

-   Se crea el output para el Egress:

    ![alt text](./img/ip_egress.png)

-	En el main.tf donde se orquestan los módulos se debe declarar este output dentro del módulo declarado de Redis cache(importante: previamente se tiene que declarar una variable dentro del modulo individual para que se pueda recoger el valor del otro modulo:

    ![alt text](./img/main_orq.png)
 
Como se observa en la imagen, Terraform nos permite obtener data de otro modulo utilizando el siguiente comando “module.{etiqueta de módulo origen}.{nombre de etiqueta de output a obtener}”. Véase: [link][19].

Adicionalmente, a partir de la version 0.13 de terraform se puede implementar las dependencias entre módulos. Vease: [link][13].

### 4.3. Variables

Como un modulo individual, el módulo orquestador también tiene un archivo ´variables.tf´ con la diferencia que además de las variables necesarios para el módulo, también se tiene que agregar “variables heredadas”.
Las variables heredadas son aquellas que han subido de los módulos individuales de la capa  1, con el objetivo de poder indicarles los datos que necesitan. Estas variables pueden llenarse en el mismo archivo `main.tf` como un dato en duro o como una variable en `variable.tf`.

Ejemplo:

-	Módulo declarado en el main.tf del módulo orquestador:

    ![alt text](./img/modulo_orq.png)
    
    Como se puede observar las dos variables tiene el mismo nombre, esto se debe cumplir para evitar mayor complejidad en los modulos.

## 5. Documentación

+ **CHANGELOG.md :** Es un archivo que contiene una lista en orden cronológico sobre los cambios que vamos haciendo en cada reléase (o versión) del módulo.
    + **¿Cúal es el propósito del change log? :** Para que les sea más fácil a los usuarios y contribuyentes, ver exactamente los cambios notables que se han hecho entre cada versión (o versiones) del proyecto.
    + **¿Cómo podemos hacer un buen change log? :** Se debe incluir lo siguiente:
        - La legibilidad es fundamental.
        - Fácil de conectar a cualquier sección.
        - Una sub-sección por versión.
        - Lista los releases en un orden inversamente-cronológico (el más reciente en la parte superior).
        - Escribe todas las fechas en formato AAAA-MM-DD.
        - Cada versión debería:
            + Indicar su fecha de lanzamiento en el formato anterior.
            + Agrupar los cambios para describir su impacto en el módulo, de la siguiente manera:
            + `Added` para funcionalidades nuevas.
            + `Changed` para los cambios en las funcionalidades existentes.
            + `Deprecated` para indicar que una característica está obsoleta y que se eliminará en las próximas versiones.
            + `Removed` para las características en desuso que se eliminaron en esta versión.
            + `Fixed` para correcciones y bugs.
            + `Security` para los cambios asociado a la Línea Base de Seguridad.
        - Ver plantilla [CHANGELOG.md][11].

+ **README.md :** Es un archivo que contiene una descripción del módulo y para que se debe utilizar.
    - Se debe incluir el detalle de las entradas y salidas que soporta el módulo, sus valores permitidos y el formato de ingreso segun el tipo de variable.
    - Ver plantilla [README.md][12].


## 6. Continuous Integration Test Pipelines

> ... En Desarrollo ... 
## 7. Links de Referencia

+ Uso de for_each: https://github.com/hashicorp/terraform/issues/17179
+ Mejores prácticas terraform: https://www.hashicorp.com/blog/announcing-terraform-recommended-practices/
+ Ventas de usar for_each: https://github.com/hashicorp/terraform/issues/17179
+ Listas, Mapas y objects: https://www.hashicorp.com/blog/terraform-0-12-rich-value-types
+ Uso de los diferentes tipos de variables en terraform: https://upcloud.com/community/tutorials/terraform-variables/
+ Como acceder a los datos de las variables de tipo complejo: https://www.terraform.io/docs/configuration/expressions.html#references-to-resource-attributes
+ Estructura para creación de recursos: https://discuss.hashicorp.com/t/for-each-flattened-list/2494/4 
+ Uso de Loops y condiciones: https://blog.gruntwork.io/terraform-tips-tricks-loops-if-statements-and-gotchas-f739bbae55f9
+ Uso del for: https://www.terraform.io/docs/configuration/expressions.html#for-expressions





[1]: https://bitbucket.lima.bcp.com.pe/projects/IAAC/repos/azu-msql-module/browse/devops/jenkins
[2]: https://www.terraform.io/docs/cloud/registry/index.html
[3]: https://www.terraform.io/docs/cloud/registry/index.html
[4]: https://confluence.lima.bcp.com.pe/display/DB/MVP
[5]: https://semver.org/
[6]: ./documentacion/README.md
[7]: ./documentacion/CHANGELOG.md
[8]: https://confluence.lima.bcp.com.pe/pages/viewpage.action?pageId=525501996
[9]: https://confluence.lima.bcp.com.pe/display/ADT/ARQUETIPOS+Y+ARQUITECTURAS+DE+REFERENCIA
[10]: https://www.hashicorp.com/blog/hashicorp-terraform-0-12-preview-for-and-for-each 
[11]: https://confluence.lima.bcp.com.pe/display/ADT/Lista+de+paginas+-+Arquitectura+Cloud
[12]: http://ggeneral.bcp.com.pe/sites/gsti/seg/_layouts/15/start.aspx#/Lineas/Base/de/Seguridad/Forms/AllItems.aspx?RootFolder=%2fsites%2fgsti%2fseg%2fLineas/Base/de/Seguridad%2fMicrosoft%2fAzure%2fPaaS&FolderCTID=0x012000E62958EB0F7E0348AFDDFC7FB304D5E6
[13]: https://medium.com/hashicorp-engineering/creating-module-dependencies-in-terraform-0-13-4322702dac4a
[14]: https://www.terraform.io/docs/commands/fmt.html
[15]: https://www.terraform.io/docs/configuration/style.html
[16]: https://confluence.lima.bcp.com.pe/display/ADT/Nomenclaturas+Cloud 
[17]: https://www.terraform.io/docs/configuration/variables.html
[18]: https://wappeu2e195p01.azurewebsites.net/portafolioaplicaciones/Listado
[19]: https://www.terraform.io/docs/configuration/modules.html#accessing-module-output-values

